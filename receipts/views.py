from django.shortcuts import render, redirect
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def receipts_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": receipt,
    }
    return render(request, "receipts/home.html", context)
